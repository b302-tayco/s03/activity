/*USERS*/
INSERT INTO users(email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-1-1 01:00:00"  );
INSERT INTO users(email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-1-1 02:00:00"  );
INSERT INTO users(email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-1-1 03:00:00"  );
INSERT INTO users(email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-1-1 04:00:00"  );
INSERT INTO users(email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-1-1 05:00:00"  );


/*POSTS*/

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "First Code","Hello World!", "2021-01-02 01:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "Second Code","Hello Earth!", "2021-01-02 02:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (2, "Third Code","Welcom to Mars!", "2021-01-02 03:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (4, "Fourth Code","Bye Bye solar system!", "2021-01-02 04:00:00");


/*Author ID*/
SELECT * FROM posts WHERE author_id = 1;

/*User's emails and date*/
SELECT email, datetime_created from users; 

/*UPDATE*/
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 1;

/*DELETE*/

DELETE FROM users WHERE email = "johndoe@gmail.com";
